<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>No Password Failed Login BDD</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>5a9b5b8b-b585-40b9-aac2-84af024f1be2</testSuiteGuid>
   <testCaseLink>
      <guid>36c1b2d5-91f4-4bc2-a344-ca1f86f73a49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_Failed_NoPassword</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>00c921c5-2d5c-4d1a-a5a2-d000af10a698</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/noPassword</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>00c921c5-2d5c-4d1a-a5a2-d000af10a698</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>USERNAME</value>
         <variableId>49736327-03fa-4e26-881c-b7465a0a30d3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>00c921c5-2d5c-4d1a-a5a2-d000af10a698</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>PASSWORD</value>
         <variableId>7a92accc-59b6-4952-860a-75df50f9518c</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>

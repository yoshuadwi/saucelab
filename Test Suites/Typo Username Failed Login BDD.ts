<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Typo Username Failed Login BDD</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>e8cff85d-94e9-4847-9502-9bdd12c829bb</testSuiteGuid>
   <testCaseLink>
      <guid>6382ee5a-7b8e-4e03-b5b4-3e46ec359f52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_Failed_TypoUsername</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>9ddc7ee0-32c5-4a53-9070-201927e6fe6e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/typoUsername</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>9ddc7ee0-32c5-4a53-9070-201927e6fe6e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>USERNAME</value>
         <variableId>5664fec8-4846-455f-9010-37341a6a2722</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>9ddc7ee0-32c5-4a53-9070-201927e6fe6e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>PASSWORD</value>
         <variableId>d7a8bad6-827f-4d3a-b7c2-26c0c5d27c21</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>

<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Typo Password Failed Login BDD</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>d81f9d6d-001d-461e-b767-0094fbf8e34c</testSuiteGuid>
   <testCaseLink>
      <guid>11415c73-9456-4c7a-a152-f68787e5537d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_Failed_TypoPassword</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>92eff0ea-d83b-413e-ac9c-afa8f4b151b8</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TypoPassword</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>92eff0ea-d83b-413e-ac9c-afa8f4b151b8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>USERNAME</value>
         <variableId>d7a39624-b405-4c18-850c-d7a6764bce3c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>92eff0ea-d83b-413e-ac9c-afa8f4b151b8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>PASSWORD</value>
         <variableId>29bbed20-3dea-4076-afe2-f3506172a422</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
